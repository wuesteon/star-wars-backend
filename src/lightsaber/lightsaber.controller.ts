import { Controller, Get, Post, Body, Param, Put, Delete } from '@nestjs/common';
import { LightsaberService } from './lightsaber.service';
import { Lightsaber } from './lightsaber.entity';

@Controller('lightsaber')
export class LightsaberController {
    constructor(private lightsaberService: LightsaberService) { }
    @Get()
    getAll() {
        return this.lightsaberService.getAll();
    }
    @Post()
    add(@Body() lightsaber: Lightsaber) {
        return this.lightsaberService.add(lightsaber);
    }
    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.lightsaberService.get(id);
    }

    @Put(':id')
    update(@Param('id') id: string, @Body() lightsaber: Lightsaber) {
        return this.lightsaberService.update(id, lightsaber);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.lightsaberService.remove(id);
    }
}
