import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Lightsaber {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    name: string;
    @Column()
    color: string;
    @Column()
    length: number;
}
